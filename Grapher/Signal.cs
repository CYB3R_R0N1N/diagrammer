﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Grapher
{
    struct grPoint
    {
        public int point;
        public int rf;
        
        public grPoint(int point, int rf)
        {
            this.point = point;
            this.rf = rf;
        }
    }

    class Signal
    {
        SignalContainer container;
        PictureBox drawRegion;
        List<grPoint> grPoints;

        Point location;

        int last_width = 0;
        float delta = 5;
        const float snap_rf = 10;
        const float snap_grid = 15;

        bool snap_on = false;
        bool is_grapped = false;

        int grapped = 0;
        int divide = 0;


        public event EventHandler Deleting;

        public Signal(Point location, int divide)
        {
            grPoints = new List<grPoint>();
            drawRegion = new PictureBox();
            container = new SignalContainer();

            this.location = location;
            this.divide = divide;
        }
        /// <summary>
        /// Создает графичский обьект
        /// </summary>
        /// <param name="parentBox"></param>
        public void InitDraw(PictureBox parentBox)
        {
            drawRegion.Location = new Point(-3, location.Y - 3);
            drawRegion.Anchor = ((System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right));
            drawRegion.Size = new Size(parentBox.Width, 80);
            drawRegion.BackColor = Color.Transparent;
            drawRegion.Paint += new PaintEventHandler(drawRegion_Paint);
            drawRegion.MouseDown += new MouseEventHandler(drawRegion_MouseDown);
            drawRegion.Resize += new EventHandler(drawRegion_Resize);
            drawRegion.MouseMove += new MouseEventHandler(drawRegion_MouseMove);
            drawRegion.MouseUp += new MouseEventHandler(drawRegion_MouseUp);
            drawRegion.Cursor = Cursors.VSplit;
            last_width = drawRegion.Width;
            parentBox.Controls.Add(drawRegion);
        }
        /// <summary>
        /// Создаёт контейнер сигнала
        /// </summary>
        /// <param name="parentPanel"></param>
        public void InitContainer(Panel parentPanel)
        {
            container.Location = location;
            container.Size = new System.Drawing.Size(178, 80);
            container.Closing += new System.EventHandler(signalContainer_Closing);
            container.SnapChanged += new EventHandler(signalContainer_SnapChanged);
            parentPanel.Controls.Add(container);
        }


        private void signalContainer_SnapChanged(object sender,EventArgs e )
        {
            var box = sender as CheckBox;
            if (box.CheckState == CheckState.Checked)
                snap_on = true;
            else
                snap_on = false;
        }

        private void drawRegion_Paint(object sender, PaintEventArgs e)
        {
            Draw(e.Graphics);
        }

        private void drawRegion_MouseDown(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < grPoints.Count; i++)
            {
                if (e.X < grPoints[i].point + snap_rf && e.X > grPoints[i].point - snap_rf)
                {
                    grapped = i;
                    is_grapped = true;
                    return;
                }
            }

            
        }

        private void drawRegion_MouseUp(object sender, MouseEventArgs e)
        {
            is_grapped = false;
        }

        private void drawRegion_MouseMove(object sender, MouseEventArgs e)
        {
            if (is_grapped)
                grPoints[grapped] = new grPoint(e.X,grPoints[grapped].rf);
            Update();

            //Snap prototype
            if (is_grapped && snap_on)
            {
                for (int i = 4; i < drawRegion.Width ; i += drawRegion.Width / divide)
                    if (e.X > i - snap_grid && e.X < i + snap_grid)
                        grPoints[grapped] = new grPoint(i, 0);
            }
        }



        /// <summary>
        /// Производит отрисовку диаграммы
        /// </summary>
        /// <param name="g"></param>
        public void Draw(Graphics g)
        {
            int amplitude = 50;
            int spacing = 10;
            bool is_rise = true;
            Pen pen = new Pen(Color.Black, 3);

            g.DrawLine(new Pen(Color.Black, 1), 0, drawRegion.Height - spacing, drawRegion.Width, drawRegion.Height - spacing);

            for (int i = 0; i < grPoints.Count; i++)
            {
                if (i == 0)
                {
                    g.DrawLine(pen, 0, drawRegion.Height - spacing, grPoints[i].point - grPoints[i].rf / 2, drawRegion.Height - spacing);
                    g.DrawLine(pen, grPoints[i].point - grPoints[i].rf / 2, drawRegion.Height - spacing, grPoints[i].point + grPoints[i].rf / 2, drawRegion.Height - spacing - amplitude);
                    is_rise = !is_rise;
                    continue;
                }

                if (is_rise)
                {
                    g.DrawLine(pen, grPoints[i - 1].point + grPoints[i- 1].rf / 2, drawRegion.Height - spacing, grPoints[i].point - grPoints[i].rf / 2, drawRegion.Height - spacing);
                    g.DrawLine(pen, grPoints[i].point - grPoints[i].rf / 2, drawRegion.Height - spacing, grPoints[i].point + grPoints[i].rf / 2, drawRegion.Height- spacing - amplitude );
                    is_rise = !is_rise;
                }
                else
                {
                    g.DrawLine(pen, grPoints[i - 1].point+ grPoints[i-1].rf / 2, drawRegion.Height - spacing - amplitude, grPoints[i].point - grPoints[i].rf / 2, drawRegion.Height - spacing - amplitude);
                    g.DrawLine(pen, grPoints[i].point - grPoints[i].rf / 2, drawRegion.Height - spacing - amplitude, grPoints[i].point + grPoints[i].rf / 2, drawRegion.Height - spacing);
                    is_rise = !is_rise;
                }

                if (i == grPoints.Count -1)
                {
                    if (is_rise)
                    {
                        g.DrawLine(pen, grPoints[i].point + grPoints[i].rf / 2, drawRegion.Height - spacing, drawRegion.Width, drawRegion.Height - spacing);
                    }
                    else
                    {
                        g.DrawLine(pen, grPoints[i].point + grPoints[i].rf / 2, drawRegion.Height - spacing - amplitude, drawRegion.Width, drawRegion.Height - spacing - amplitude);
                    }
                }

            }
        }
        /// <summary>
        /// Производит отображение рисунка
        /// </summary>
        public void Update()
        {
            drawRegion.Invalidate();
        }
        /// <summary>
        /// Добавление точки в список точек роста
        /// </summary>
        /// <param name="point"></param>
        public void AddPoint(int point, int rf)
        {
            for (int i = 0; i < grPoints.Count; i++)
            {
                if (point < grPoints[i].point + delta && point > grPoints[i].point - delta)
                    return;
                if (point > grPoints[i].point)
                    continue;
                else
                {
                    grPoints.Insert(i, new grPoint(point, 0));
                    return;
                }
            }
            grPoints.Add(new grPoint(point, 0));
        }
        /// <summary>
        /// Масштабирует временную диаграмму
        /// </summary>
        /// <param name="divide"></param>
        public void Rescale(int divide)
        {
            if (divide > 0)
                this.divide = divide;
            else
                this.divide = -divide;

            for (int i = 0; i < grPoints.Count; i++)
                grPoints[i] = new grPoint(grPoints[i].point + grPoints[i].point / divide, grPoints[i].rf + grPoints[i].rf/ divide);
        }
        /// <summary>
        /// Масштабирование диаграммы при изменении размера формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void drawRegion_Resize(object sender, EventArgs e)
        {
            if (last_width - drawRegion.Width == 0)
                return;
            if (drawRegion.Width == 0)
                return;
            for (int i = 0; i < grPoints.Count; i++)
                grPoints[i] = new grPoint(grPoints[i].point + grPoints[i].point * (drawRegion.Width - last_width) / last_width, grPoints[i].rf + grPoints[i].rf * (drawRegion.Width - last_width) / last_width);
            //Добавить масштабирование фронтов импульса
            last_width = drawRegion.Width;
            Update();
        }
        /// <summary>
        /// Обработчик события удаления сигнала
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signalContainer_Closing(object sender, EventArgs e)
        {
            if (Deleting != null)
                Deleting(this, e);
            drawRegion.Parent.Controls.Remove(drawRegion);
            drawRegion.Dispose();
            Update();
        }
        /// <summary>
        /// Location getter&setter
        /// </summary>
        public Point Location
        {
            get { return this.location; }
            set { this.location = value; }
        }

        public void pP()
        {
            for (int i = 0; i < grPoints.Count; i++)
                Console.WriteLine(grPoints[i].point);
        }
        public void AddPoints()
        {
            grPoints.Add(new grPoint(30, 0));
            grPoints.Add(new grPoint(100, 0));
            grPoints.Add(new grPoint(300, 0));
            grPoints.Add(new grPoint(500, 0));
            grPoints.Add(new grPoint(650, 0));
        }

    }
}
