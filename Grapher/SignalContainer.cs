﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grapher
{
    public partial class SignalContainer : UserControl
    {
        public event EventHandler Closing;
        public event EventHandler SnapChanged;

        public SignalContainer()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Panel panel = this.Parent as Panel;
            if (Closing != null)
                Closing(this, e);
            this.Parent.Controls.Remove(this);
            panel.Refresh();
            this.Dispose();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (SnapChanged != null)
                SnapChanged(sender, e);
        }
    }
}
