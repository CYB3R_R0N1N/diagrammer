﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grapher
{
    public partial class Form1 : Form
    {
        Signal s;

        List<Signal> Signals = new List<Signal>();
        int divide = 10;

        public Form1()
        {
            InitializeComponent();
            s = new Signal(new Point(3,3), divide);
            s.InitDraw(pictureBox1);
            s.InitContainer(panel2);
            s.Deleting += new EventHandler(signalDeleting);
            Signals.Add(s);
            s.AddPoints();
            s.AddPoint(220, 10);
            s.AddPoint(300, 20);
            s.AddPoint(270, 20);
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {

            s.Update();

            if (e.Button == MouseButtons.Right)
                contextMenuStrip1.Show(sender as PictureBox, e.X, e.Y);
        }


        ///////////Drawning functions /////////////////////
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            DrawGrid(e.Graphics);
        }

        private void DrawGrid(Graphics g)
        {
            for (float i = 1; i < pictureBox1.Size.Width; i+= pictureBox1.Size.Width / divide)
                g.DrawLine(new Pen(Color.Black, 1), i, 0, i, pictureBox1.ClientSize.Height);
        }

        private void pictureBox1_Resize(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
        }

        private void addline(object sender, EventArgs e)
        {

            addline_button.Location = new Point(addline_button.Location.X, addline_button.Location.Y + 70 + 5);
            this.Refresh();
        }

        private void savepic(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(panel2.Width + panel3.Width, panel2.Height);
            panel2.DrawToBitmap(bitmap, new Rectangle(Point.Empty, panel2.Size));
            panel3.DrawToBitmap(bitmap, new Rectangle(new Point(panel2.Width,0), panel3.Size));

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "jpg files (*.jpg)|*.jpg|png files (*.png)|*.png|bmp files (*.bmp)|*.bmp";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    bitmap.Save(saveFileDialog.FileName);
                }
                catch
                {
                    MessageBox.Show("Unable to save picture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void resizedec(object sender, EventArgs e)
        {
            divide++;
            s.Rescale(-divide);
            s.Update();
            pictureBox1.Invalidate();
        }

        private void resizeinc(object sender, EventArgs e)
        {
            if (divide == 1)
                return;
            divide--;
            s.Rescale(divide);
            s.Update();
            pictureBox1.Invalidate();
        }

        private void signalDeleting(object sender, EventArgs e)
        {
            Signals.Remove(s);
            Console.WriteLine("Container closing/ Class deleting");
        }
    }
}
